
<!-- README.md is generated from README.Rmd. Please edit that file -->

<img src=figures/logo.png align="right" width="12%" hspace="50">

## linkScraping: Creating a .bib library from journal html pages

The goal of linkScraping is to scrap the journal pages and create a .bib
library.

It uses python to scrap and R to organize data.

### Scripts in order of use

  - py/pyScrap.py : gets links of every issue in a raw format

<!-- end list -->

``` 
[1] "https://www.investigacionyciencia.es/revistas/cuadernos/inteligencia-humana-802"
[2] "https://www.investigacionyciencia.es/revistas/cuadernos/estrs-790"              
```

  - py/pyScrapDownFields.py : Opens links and downloads article info as
    dictionary and pandas (.csv)

<!-- end list -->

``` r
df2020<-data.table::fread("csv/2020.csv")
knitr::kable(head(df2020))
```

| year | issue                    | area          | title                                       | author                                       | abstract                                                                                                                                                                                                                          |
| ---: | :----------------------- | :------------ | :------------------------------------------ | :------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 2020 | ENERO/ABRIL 2020 · Nº 25 |               | Riesgo de combustión                        | La redacción                                 |                                                                                                                                                                                                                                   |
| 2020 | ENERO/ABRIL 2020 · Nº 25 | NEUROCIENCIA  | El estrés deja su huella molecular          | Eric J. Nestler                              | Los traumas afectan a cada persona de forma distinta. El motivo de tal diversidad reside, en parte, en la epigenética.                                                                                                            |
| 2020 | ENERO/ABRIL 2020 · Nº 25 | BIOPSICOLOGÍA | El rastro genético del desgaste profesional | Martin Reuter                                | El síndrome de desgaste profesional (burnout) suele interpretarse como una consecuencia del estrés y de otros factores externos. Investigaciones recientes revelan que los genes también intervienen.                             |
| 2020 | ENERO/ABRIL 2020 · Nº 25 | DERMATOLOGÍA  | Nervios a flor de piel                      | Angelika Bauer-Delto                         | Los trastornos psicológicos pueden ­empeorar algunas enfermedades cutáneas de manera virulenta. Las preocupaciones, las tensiones y el enojo agravan el eccema atópico, además de otras inflamaciones dermatológicas, pero ¿cómo? |
| 2020 | ENERO/ABRIL 2020 · Nº 25 | PSIQUIATRÍA   | Estrés: Diferencias entre sexos             | Debra A. Bangasser                           | La biología de este trastorno difiere en los machos y las hembras. ¿Qué implicaciones podría tener esto a la hora de tratar el estrés postraumático, la depresión y otras enfermedades mentales?                                  |
| 2020 | ENERO/ABRIL 2020 · Nº 25 | NEUROCIENCIA  | El cerebro sometido a tensión               | Amy Arnsten, Carolyn M. Mazure, Rajita Sinha | Los circuitos neuronales responsables del autocontrol consciente son sumamente vulnerables al estrés. Cuando no funcionan, la mente es incapaz de regular los impulsos primarios y se paraliza.                                   |

  - py/pyScrapDownPdf\_safe.py: Opens links and downloads .pdfs (needs
    account)

  - R/csvTobib : After having a .csv transforms to .bib

  - R/updateIndex: Creates .csv and .xlsx
