---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```
```{r, echo=F,  results="asis"}
img1_path <- "figures/logo.png"
if(file.exists(img1_path)) {
cat(paste0("<img src=",img1_path," align=\"right\" width=\"12%\" hspace=\"50\">") )
}
```

## linkScraping: Creating a .bib library from journal html pages

The goal of linkScraping is to scrap the journal pages and create a .bib library.

It uses python to scrap and R to organize data.

### Scripts in order of use

- py/pyScrap.py : gets links of every issue in a raw format

```{r, echo = F, comment=NA}
mylinks2020<-readLines("sourcelinks/2020.txt")
head(mylinks2020)
```

- py/pyScrapDownFields.py : Opens links and downloads article info as dictionary and pandas (.csv)

```{r pressure, echo = TRUE}
df2020<-data.table::fread("csv/2020.csv")
knitr::kable(head(df2020))
```

- py/pyScrapDownPdf_safe.py: Opens links and downloads .pdfs (needs account)

- R/csvTobib : After having a .csv transforms to .bib 

- R/updateIndex: Creates .csv and .xlsx





