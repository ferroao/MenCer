#
# update decades files
#

head(articles[,1:6])

articles<- articles[order(-as.numeric(articles$NUMBER),as.numeric(articles$PAGES)),]
head(articles[,1:6])

selection<-"2020s"

unique(articles$BIBTEXKEY)
articles$BIBTEXKEY <- make.unique(articles$BIBTEXKEY)

decadeF<-unique(articles$DECADE) 
decadeF
library(xlsx)

for (i in decadeF ) {
# for (i in selection ) {
    # print(i)
    # i<-decadeF[1]
    print(paste0(githome,i,"/index",i,".csv" ) )
    write.csv( articles[which(articles$DECADE==i),],paste0(githome,i,"/index",i,".csv") )
    {
        wb <- createWorkbook(type = "xlsx")
        sheet <- createSheet(wb, sheetName = "all")
        addDataFrame(articles[which(articles$DECADE==i),], sheet, row.names = FALSE)
        autoSizeColumn(sheet, colIndex = 1:ncol(articles) )
        setColumnWidth(sheet, colIndex = c(1:2,8:10,12:ncol(articles)), colWidth = 12)
        setColumnWidth(sheet, colIndex = 7, colWidth = 6*12)
        saveWorkbook(wb, paste0(githome,i,"/index",i,".xlsx") )
    }
}


##################################################################################################

articles$FILEBASE<-gsub(".*/(.*/.*)\\.pdf","\\1",articles$FILE)
unique(articles$FILEBASE)

# articles$FILEBASE<-sub(".pdf$","",articles$FILEBASE) # IMPORTANT
selection<-unique(articles$FILEBASE)
selection

# selection<-unique(articles$FILEBASE)[1]

#
# update issue files
#

for (i in selection ){
    print(i)
    # i<-selection[1]
    print(paste0(githome,i,".csv" ) )
    write.csv( articles[which(articles$FILEBASE==i),], paste0(githome,i,".csv"  ) )
    # write.xlsx(articles[which(articles$FILEBASE==i),], paste0(githome,i,".xlsx" ) )
    {
        wb <- createWorkbook(type = "xlsx")
        sheet <- createSheet(wb, sheetName = "all")
        addDataFrame(articles[which(articles$FILEBASE==i),], sheet, row.names = FALSE)
        autoSizeColumn(sheet, colIndex = 1:ncol(articles) )
        setColumnWidth(sheet, colIndex = c(1:2,8:10,12:ncol(articles)), colWidth = 12)
        setColumnWidth(sheet, colIndex = 7, colWidth = 6*12)
        saveWorkbook(wb, paste0(githome,i,".xlsx"  )  )
    }
}

# articles <- articles[order(-articles$number,articles$CATEGORY, decreasing = F),]

#
#    sort d.f.
#

articles<- articles[order(-as.numeric(articles$NUMBER),as.numeric(articles$PAGES) ),]

head(unique(articles$filebase))


################################################################################################################
# update all time
head(articles[,1:7],100)

#check sort
(articles$NUMBER)
(articles$PAGES)

#
#   NCHAR
#

articles$ncharA<-NULL

#
#      update all csv
#

write.csv( articles, paste0(githome,"menCerArt.csv"  ), row.names = F)
# write.xlsx(articles, paste0(githome,"menCerArt.xlsx" ), row.names = F )

#
#    update all excel
#

library(xlsx)

colnames(articles)
unique(articles$PAGES)

{
wb <- createWorkbook(type = "xlsx")
sheet <- createSheet(wb, sheetName = "all")
addDataFrame(articles, sheet, row.names = FALSE)
autoSizeColumn(sheet, colIndex = 1:ncol(articles) )
setColumnWidth(sheet, colIndex = c(1:2,8:10,12:ncol(articles)), colWidth = 12)
setColumnWidth(sheet, colIndex = 7, colWidth = 6*12)
saveWorkbook(wb, paste0(githome,"menCerArt.xlsx" ) )
}

head(articles)

# bib2df::df2bib(articles,"biblio/articles.bib")

###
